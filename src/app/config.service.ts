import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class ConfigService {

  config: any;

  constructor() { }

  load() {
    console.info('load');
    return new Promise((resolve) => {
      this.config = localStorage.getItem('config');
      resolve();
    });
  }
}
