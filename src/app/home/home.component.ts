import * as PIXI from 'pixi.js/dist/pixi.js';
import Dexie from 'dexie';

import { Component, OnInit, ElementRef, ViewChild, HostListener, ViewChildren, QueryList, ChangeDetectorRef, Renderer2 } from '@angular/core';
import { MatMenuTrigger } from '../../../node_modules/@angular/material';
import { CdkPortal } from '../../../node_modules/@angular/cdk/portal';
import { Overlay, OverlayConfig, OverlayContainer } from '../../../node_modules/@angular/cdk/overlay';

export interface Point {
  x: number;
  y: number;
}

interface Image {
  id?: number;
  name?: string;
  type?: string;
  size?: number;
  data?: Uint8Array;
}

//
// Declare Database
//
class ImageDatabase extends Dexie {
  public friends!: Dexie.Table<Image, number>; // id is number in this case

  public constructor() {
      super("ImageDatabase");
      this.version(1).stores({
          friends: "++id,name,type,size,data"
      });
  }
}

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit {
  @ViewChild('canvasElement') canvasElement: ElementRef;
  @ViewChildren('matMenuTrigger') matMenuTriggers: QueryList<MatMenuTrigger>;
  matMenuTrigger: MatMenuTrigger;
  @ViewChild(CdkPortal) templatePortal: CdkPortal;
  subscription: any;

  app: PIXI.Application;
  root: PIXI.Container;
  rootScale: PIXI.Container;
  world: PIXI.Container;
  rosWorldOrigin: PIXI.Container;
  rosWorldBase: PIXI.Container;
  rosWorld: PIXI.Container;
  rosWorldScale: PIXI.Container;
  dot: PIXI.Graphics;

  width: number;
  height: number;
  resolution: number;
  rosOriginX: number;
  rosOriginY: number;

  constructor(private overlay: Overlay, overlayContainer: OverlayContainer, 
    private changeDetectorRef: ChangeDetectorRef,
    private renderer: Renderer2) {
    overlayContainer.getContainerElement().classList.add('unicorn-dark-theme');
  }

  ngOnInit() {
    //this.matMenuTrigger = this.matMenuTriggers.first;
  }

  @HostListener('mousewheel', ['$event'])
  onMouseWheel(e) {
    if (e.shiftKey) {
      var delta = e.deltaY ? -(e.deltaY) : e.wheelDelta ? e.wheelDelta : -(e.detail);
      if (delta < 0) {
        this.rootScale.scale.x = this.rootScale.scale.x + 0.1;
        this.rootScale.scale.y = this.rootScale.scale.y + 0.1;
        this.app.renderer.resize(this.width * this.rootScale.scale.x, this.height * this.rootScale.scale.y);
      } else {
        this.rootScale.scale.x = this.rootScale.scale.x - 0.1;
        this.rootScale.scale.y = this.rootScale.scale.y - 0.1;
        this.app.renderer.resize(this.width * this.rootScale.scale.x, this.height * this.rootScale.scale.y)
      }
    }
  }

  ngAfterViewInit() {
    this.matMenuTriggers.changes.subscribe((triggers: QueryList<MatMenuTrigger>) => {
      //console.info('test');
      if (triggers.last) {
        this.matMenuTrigger = triggers.last;
        this.matMenuTrigger.openMenu();
        this.changeDetectorRef.detectChanges();
        this.subscription = this.matMenuTrigger.menuClosed.subscribe(() => {
          console.info('close');
          this.templatePortal.detach();
        });
      }
    });


    this.width = 1920;
    this.height = 1200;

    let app = new PIXI.Application(this.width, this.height, {
      antialias: true,
      backgroundColor: 0x1099bb
    });
    app.renderer.autoResize = true;
    this.app = app;
    this.canvasElement.nativeElement.appendChild(app.view);

    this.resolution = 50; // 50 [pixel/m]

    // screen coordinates
    // top right corner
    var root = new PIXI.Container();
    this.root = root;
    app.stage.addChild(root);

    var rootScale = new PIXI.Container();
    this.rootScale = rootScale;
    root.addChild(rootScale);
    rootScale.scale.x = 0.5;
    rootScale.scale.y = 0.5;
    this.app.renderer.resize(1920 * this.rootScale.scale.x, 1200 * this.rootScale.scale.y);

    // world coordinates
    // bottom left corner
    // screen to world
    var world = new PIXI.Container();
    this.world = world;
    rootScale.addChild(world);
    world.setTransform(0, this.height, 1, -1, 0, 0, 0, 0, 0);

    // center origin
    var rosWorldOrigin = new PIXI.Container();
    this.rosWorldOrigin = rosWorldOrigin;
    rosWorldOrigin.position.x = (this.width / 2);
    rosWorldOrigin.position.y = (this.height / 2);
    world.addChild(rosWorldOrigin);

    // rotation
    var rosWorldBase = new PIXI.Container();
    this.rosWorldBase = rosWorldBase;
    rosWorldOrigin.addChild(rosWorldBase);
    rosWorldBase.setTransform(0, 0, 1, 1, Math.PI / 2, 0, 0, 0, 0);

    // ros coordinates
    // world to rosWorld
    var rosWorld = new PIXI.Container();
    this.rosWorld = rosWorld;
    rosWorldBase.addChild(rosWorld);

    // scaling
    var rosWorldScale = new PIXI.Container();
    this.rosWorldScale = rosWorldScale;
    rosWorldScale.scale.x = this.resolution;
    rosWorldScale.scale.y = this.resolution;
    rosWorld.addChild(rosWorldScale);

    root.updateTransform();
    rosWorldScale.updateTransform();

    let oneMeterPixel = this.resolution;

    var xAxis = new PIXI.Graphics();
    rosWorld.addChild(xAxis);
    xAxis.lineStyle(4, 0xff0000, 0.5);
    xAxis.moveTo(0, 0);
    xAxis.lineTo(0, this.height);
    xAxis.lineStyle(1, 0x000000, 0.5);
    xAxis.moveTo(0, -this.height);
    xAxis.lineTo(0, 0);
    for (let x = oneMeterPixel; x < this.width; x += oneMeterPixel) {
      xAxis.moveTo(x, -this.height);
      xAxis.lineTo(x, this.height);
    }
    for (let x = -oneMeterPixel; x > -this.width; x -= oneMeterPixel) {
      xAxis.moveTo(x, -this.height);
      xAxis.lineTo(x, this.height);
    }
    var yAxis = new PIXI.Graphics();
    rosWorld.addChild(yAxis);
    yAxis.lineStyle(4, 0x00ff00, 0.5);
    yAxis.moveTo(0, 0);
    yAxis.lineTo(this.width, 0);
    yAxis.lineStyle(1, 0x000000, 0.5);
    yAxis.moveTo(-this.width, 0);
    yAxis.lineTo(0, 0);
    for (let y = oneMeterPixel; y < this.height; y += oneMeterPixel) {
      yAxis.moveTo(-this.width, y);
      yAxis.lineTo(this.width, y);
    }
    for (let y = -oneMeterPixel; y > -this.height; y -= oneMeterPixel) {
      yAxis.moveTo(-this.width, y);
      yAxis.lineTo(this.width, y);
    }

    var dot = new PIXI.Graphics();
    rosWorld.addChild(dot);
    dot.lineStyle(0);
    dot.beginFill(0x0000FF, 0.5);
    for (let y = -this.height; y < this.height; y += oneMeterPixel) {
      dot.drawCircle(y, y, 20, 20);
    }
    dot.interactive = true;
    dot.on('pointerdown', function (event) {
      console.info('mousedown');
      let e = event.data.originalEvent;
      console.info('(' + e.offsetX + ', ' + e.offsetY + ')');
      let screenPoint = new PIXI.Point(e.offsetX, e.offsetY);
      let rosWorldScaleToScreen = rosWorldScale.worldTransform;
      let rosPoint = rosWorldScaleToScreen.applyInverse(screenPoint);
      console.info('(' + rosPoint.x + ', ' + rosPoint.y + ')');
      let sPoint = rosWorldScaleToScreen.apply(rosPoint);
      console.info('(' + sPoint.x + ', ' + sPoint.y + ')');




      rosX = 5;
      rosY = 2;
      dot.drawCircle(rosX * that.resolution, rosY * that.resolution, 20, 20);

      let rosWorldToScreen = rosWorld.worldTransform;
      let rosPointPixel = rosWorldToScreen.applyInverse(screenPoint);
      console.info('(' + rosPointPixel.x + ', ' + rosPointPixel.y + ')');
      that.rosWorld.position.x = -rosPointPixel.x;
      that.rosWorld.position.y = -rosPointPixel.y;
    });
    let rosX = 2;
    let rosY = 5;
    dot.drawCircle(rosX * this.resolution, rosY * this.resolution, 20, 20);

    for (let y = -this.height; y < this.height; y += oneMeterPixel) {
      let meter = y / oneMeterPixel;
      var basicText = new PIXI.Text(meter + '');
      basicText.x = y;
      basicText.y = y;
      basicText.scale.x = -1;
      basicText.rotation = Math.PI / 2;
      rosWorld.addChild(basicText);

      // var text = new PIXI.Text(meter + 't');
      // var scaleText = new PIXI.Text(meter + 's');
      // let rosPoint = new PIXI.Point(meter, meter);
      // let transform = rosWorldScale.worldTransform;
      // let screenPoint = transform.apply(rosPoint);
      // let screenScalePoint = rootScale.localTransform.applyInverse(screenPoint);
      // text.x = screenPoint.x;
      // text.y = screenPoint.y;
      // root.addChild(text);
      // scaleText.x = screenScalePoint.x;
      // scaleText.y = screenScalePoint.y;
      // rootScale.addChild(scaleText);
    }

    let arrow = new PIXI.Graphics();
    rosWorld.addChild(arrow);
    let size = 24;
    arrow.beginFill(0xFF3300, 0.5);
    arrow.lineStyle(4, 0x000000, 0.5);
    arrow.moveTo(-size / 2.0, -size / 2.0);
    arrow.lineTo(size, 0);
    arrow.lineTo(-size / 2.0, size / 2.0);
    arrow.closePath();
    arrow.endFill();
    arrow.rotation = Math.PI / 4;
    let count = 0;
    app.ticker.add(function (delta) {
      if (count++ % 60 == 0) {
        arrow.x += Math.random() * 100 - 50;
        arrow.y += Math.random() * 100 - 50;
        arrow.rotation += Math.random() * Math.PI / 2 - Math.PI / 4;
      }
    });

    var graphics = new PIXI.Graphics();


    graphics.interactive = true;

    let that = this;
    graphics
      // Mouse & touch events are normalized into
      // the pointer* events for handling different
      // button events.
      .on('pointerdown', function (event) {
        console.info('mousedown');
        let e = event.data.originalEvent;

        console.info('(' + e.offsetX + ', ' + e.offsetY + ')');

        if (e.shiftKey) {
          if (that.canvasElement.nativeElement.webkitRequestFullscreen) {
            that.canvasElement.nativeElement.webkitRequestFullscreen();
          }
        } else {
          const positionStrategy = that.overlay.position()
            .global().left(e.pageX + 'px').top(e.pageY + 'px');
          const scrollStrategy = that.overlay.scrollStrategies.close();
          const config = new OverlayConfig({
            scrollStrategy,
            hasBackdrop: true,
            positionStrategy,
          });
          const overlayRef = that.overlay.create(config);
          overlayRef.backdropClick().subscribe(event => that.templatePortal.detach());
          that.templatePortal.attach(overlayRef);
        }

        //that.root.scale.x = that.root.scale.x - 0.1;      
        //that.root.scale.y = that.root.scale.y - 0.1;
        //that.app.renderer.resize(1920 * that.root.scale.x, 1200 * that.root.scale.y)
      });

    // set a fill and line style
    graphics.beginFill(0xFF3300);
    graphics.lineStyle(4, 0xffd900, 1);

    // draw a shape
    graphics.moveTo(50, 50);
    graphics.lineTo(250, 50);
    graphics.lineTo(100, 100);
    graphics.lineTo(50, 50);
    graphics.endFill();

    // set a fill and a line style again and draw a rectangle
    graphics.lineStyle(2, 0x0000FF, 1);
    graphics.beginFill(0xFF700B, 1);
    graphics.drawRect(50, 250, 120, 120);

    // draw a rounded rectangle
    graphics.lineStyle(2, 0xFF00FF, 1);
    graphics.beginFill(0xFF00BB, 0.25);
    graphics.drawRoundedRect(150, 450, 300, 100, 15);
    graphics.endFill();

    // draw a circle, set the lineStyle to zero so the circle doesn't have an outline
    graphics.lineStyle(0);
    graphics.beginFill(0xFFFF0B, 0.5);
    graphics.drawCircle(470, 90, 60);
    graphics.endFill();

    app.stage.addChild(graphics);
  }

  rosMeterToRosPixel(p: Point): Point {
    return { x: p.x * this.resolution, y: p.y * this.resolution } as Point;
  }

  settings() {
    const db = new ImageDatabase();
    db.transaction('rw', db.friends, async() => {
      const files = await db.friends.toArray();
      console.info(files);
      for (let i = 0; i < files.length; i++) {
        const imgElement = this.renderer.createElement('img');
        var blob = new Blob([ files[i].data ], { type: files[i].type });
        imgElement.src = window.URL.createObjectURL(blob);
        this.renderer.appendChild(document.body, imgElement);  
      }
    }).catch(e => {
        console.error(e.stack || e);
    });

    const inputElement = this.renderer.createElement('input');
    inputElement.type = 'file';
    inputElement.multiple='multiple';
    inputElement.accept='image/*';
    //this.renderer.appendChild(document.body, inputElement);
    inputElement.addEventListener("change", this.onChange, false);
    inputElement.click();
    //this.renderer.removeChild(document.body, inputElement);
  }

  onChange(event: any) {
    let files = event.currentTarget.files;
    let file = files[0];
    let reader = new FileReader();
    reader.onload = function (event: any) {
      let arrayBuffer : ArrayBuffer = reader.result as ArrayBuffer;
      let data = new Uint8Array(arrayBuffer);

      const db = new ImageDatabase();
      db.transaction('rw', db.friends, async() => {
          // Make sure we have something in DB:
          if ((await db.friends.where({name: file.name}).count()) === 0) {
              const id = await db.friends.add({
                name: file.name,
                type: file.type,
                size: file.size,
                data: data
                });
          }
      }).catch(e => {
          console.error(e.stack || e);
      });
      console.info(reader.result);  
    }
    reader.readAsArrayBuffer(file);
  }
}
